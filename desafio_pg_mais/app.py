from flask import Flask
from flask_restful import Api
from resources.message import Message, MessagesLiberadas, MessagesBloqueadas

# carrega as confiracoes do banco e mapeamento de tabela para inicializar
app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///banco.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

api = Api(app)

# TODO:: Carregar todo tipo de parametro por .Env
# TODO:: Condigurar Docker

# senao exister banco , criar um baseado nos parametros do SQLALchemy, nesse caso usando SQLlite
@app.before_first_request
def cria_banco():
    banco.create_all()


# adiciona os recursos que serao usados na API

api.add_resource(Message, '/message')
api.add_resource(MessagesLiberadas, '/messages/liberadas')
api.add_resource(MessagesBloqueadas, '/messages/bloqueadas')

if __name__ == '__main__':  # inicializa o banco e sobe o servico flask
    from sql_alchemy import banco

    banco.init_app(app)
    app.run(debug=True)
