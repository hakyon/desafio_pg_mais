from sql_alchemy import banco

import requests


class MessageModel(banco.Model):
    __tablename__ = 'messages'

    #inicializa a tabela associado ao objeto e suas colunas
    id_messagem = banco.Column(banco.String, primary_key=True)
    ddd = banco.Column(banco.Integer())
    celular = banco.Column(banco.Integer())
    operadora = banco.Column(banco.Integer())
    horario_envio = banco.Column(banco.String)
    menssagem = banco.Column(banco.String(140))
    bloqueio = banco.Column(banco.Boolean()) # esse atributo servira apenas para listar as mensagens bloqueadas e liberadas

    def __init__(self, id_messagem, ddd, celular, operadora, horario_envio, menssagem):
        self.id_messagem = id_messagem
        self.ddd = ddd
        self.celular = celular
        self.operadora = operadora
        self.horario_envio = horario_envio
        self.menssagem = menssagem
        self.bloqueio = None

    def json(self):
        """
        @describe : metodo para montagem padrao de retorno de um JSON em caso de sucesso no armazenamento de
        uma menssagem valida
        @param self:
        @return: JSON
        """

        # verifica o broker de acordo com a operadora
        broker = self.check_broker(self.operadora)

        # TODO::melhorar esse objeto de retorno, muito cast sendo ultilizado ali
        return {
            'id_messagem': self.id_messagem,
            'ddd': self.ddd,
            'celular': self.celular,
            'operadora': self.operadora,
            'horario_envio': self.horario_envio,
            'menssagem': self.menssagem,
            'bloqueio': self.bloqueio,
            'mensagem_unificada': self.id_messagem + ';' + str(self.ddd) + ';' + str(self.celular) + ';' + str(self.operadora) + ';' + str(self.horario_envio) + ';' + str(self.menssagem),
            'message_return': self.id_messagem + ';' + str(broker)
        }

    @classmethod
    def find_message(cls, id_messagem):
        message = cls.query.filter_by(id_messagem=id_messagem).first()
        if message:
            return message
        return None

    @classmethod
    def find_message_liberadas(cls):
        message = cls.query.filter_by(bloqueio=False)
        if message:
            return message
        return None

    @classmethod
    def find_message_bloqueadas(cls):
        message = cls.query.filter_by(bloqueio=True)
        if message:
            return message
        return None

    @classmethod
    def check_broker(cls, operadora):

        if operadora == 'VIVO' or operadora == 'TIM':
            return 1
        elif operadora == 'CLARO' or operadora == 'OI':
            return 2
        elif operadora == 'NEXTEL':
            return 3

    @classmethod
    def validar_celular(self, ddd, celular):
        """
        @describe : metodo para verificar se o numero eh valido de acordo com os requisitos informados no desafio proposto,
        @param self:
        @param ddd: ddd do telefone
        @param celular: apenas o numero do celular
        @return: boolean
        """
        #  DDD com 2 digitos e DDD deve ser válido e  tamanho do numero deve ser 9 e numero deve começar com 9 e o segundo dígito deve ser 6
        if len(ddd) == 2 and self.check_ddd(ddd) and len(celular) == 9 and int(celular[0]) == 9 and int(celular[1]) > 6:
            return True
        else:
            return False

    @classmethod
    def check_ddd(self, ddd):
        """
        @describe : metodo para verificar se o ddd esta valido dentro da lista de ddd`s deo todos os estados do Brasil
        @param self:
        @param ddd: ddd do telefone a ser analisado
        @return: boolean
        """


        # sim, nao foi muito inteligente fazer assim :(
        # TODO:: Buscar uma lista pronta disso, fixo nao esta bom
        lista_ddd = [11, 12, 13, 14, 15, 16, 17, 18, 19, 21, 22, 24, 27, 28, 31, 32, 33, 34, 35, 37, 38, 41, 42, 43, 44,
                     45, 46, 47, 48, 49, 51,
                     53, 54, 55, 61, 62, 63, 64, 65, 66, 67, 68, 69, 71, 73, 74, 75, 77, 79, 81, 82, 83, 84, 85, 86, 87,
                     88, 89, 91, 92, 93, 94,
                     95, 96, 97, 98, 99]

        if int(ddd) in lista_ddd:
            return True
        else:
            return False

    @classmethod
    def check_blacklist(self, celular):
        """
        @describe : metodo para  verificar se o numero pertence a uma blacklist, isso eh feito chamando uma API externa
        e informando o celular com ddd
        @param self:
        @param celular: numero com ddd para ser analisado
        @return: boolean
        """

        resp = requests.get('https://front-test-pg.herokuapp.com/blacklist/' + celular)

        if resp.status_code != 200:
            return True
        else:
            return False

    def save_message(self):
        self.bloqueio = False
        banco.session.add(self)
        banco.session.commit()

    def save_message_com_bloqueio(self):
        self.bloqueio = True
        banco.session.add(self)
        banco.session.commit()

    def delete_message(self):
        banco.session.delete(self)
        banco.session.commit()
