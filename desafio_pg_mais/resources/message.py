from flask_restful import Resource, reqparse
from models.message import MessageModel
import sqlite3
from datetime import datetime


class Message(Resource):
    # validao de campos requeridos, nesse caso a unica chave eh message
    atributos = reqparse.RequestParser()
    atributos.add_argument('message', type=str, required=True,
                           help="Campo obrigatorio, nao pode ser deixado em branco.")

    def post(self):
        dados = Message.atributos.parse_args()

        # extrai a mensagem como unica string e separada em blocos para analise da regra de negocio
        msg = dados['message']

        message_id = msg.split(';')[0]
        ddd = msg.split(';')[1]
        celular = msg.split(';')[2]
        operadora = msg.split(';')[3]
        horario_envio = msg.split(';')[4]
        mensagem = msg.split(';')[5]

        if MessageModel.find_message(message_id):
            return {"message": "message_id '{}' ja existe.".format(message_id)}, 400  # validar message_id repetida


        # inicializa objeto mapeado com atributos armazenados
        message = MessageModel(message_id, ddd, celular, operadora, horario_envio, mensagem)

        # checar tamanho da menssagem
        if len(mensagem) > 140:
            try:
                message.save_message_com_bloqueio()
            except:
                return {"message": "Ocorreu um erro ao tentar armazenar a mesagem bloqueada."}, 500  # Falha nao prevista
            return message.json(), 201

        # verificar horario de envio
        # aqui tambem nao esta bonito, mas por enquanto serve para validar
        hora_em_texto = "19:59:59"
        hora_limite = datetime.strptime(hora_em_texto, '%H:%M:%S').time()
        horario_envio_dt = datetime.strptime(horario_envio, '%H:%M:%S').time()

        if horario_envio_dt > hora_limite:
            # return {"message": "Ultrapassou horario limite de:'{}'.".format(hora_limite)},
            try:
                message.save_message_com_bloqueio()
            except:
                return {"message": "Ocorreu um erro ao tentar armazenar a messagem bloqueada."}, 500  # Falha nao prevista
            return message.json(), 201


        # verificar se o ddd eh SP
        if ddd == 11:
            try:
                message.save_message_com_bloqueio()
            except:
                return {"message": "Ocorreu um erro ao tentar armazenar a mesagem bloqueada."}, 500  # Falha nao prevista
            return message.json(), 201

        # TODO:: Emcapsular melhor por aqui, esta muito verboso

        # checar validacao do telefone
        if MessageModel.validar_celular(ddd, celular):
            if MessageModel.check_blacklist(ddd + celular): # senao estiver na black lista , ok
                try:
                    message.save_message()
                except:
                    return {"message": "Ocorreu um erro ao tentar armazenar a messagem."}, 500  # Falha nao prevista
                return message.json(), 201
            else:
                try:
                    message.save_message_com_bloqueio()
                except:
                    return {"message": "Ocorreu um erro ao tentar armazenar a messagem bloqueada."}, 500  # Falha nao prevista
                return message.json(), 201
        else:
            try:
                message.save_message_com_bloqueio()
            except:
                return {"message": "Ocorreu um erro ao tentar armazenar a messagem bloqueada."}, 500  # Falha nao prevista
            return message.json(), 201


class MessagesLiberadas(Resource):
    # TODO :: Remover redundancias aqui nessas duas chamadas abaixc

    def get(self):
        connection = sqlite3.connect('banco.db')
        cursor = connection.cursor()

        resultado = cursor.execute('SELECT * FROM messages WHERE bloqueio = 0')

        if resultado:
            messages = []
            for linha in resultado:
                messages.append({
                    'message_id': str(linha[0]),
                    'ddd': str(linha[1]),
                    'celular': str(linha[2]),
                    'operadora': str(linha[3]),
                    'horario_envio': str(linha[4]),
                    'mensagem': str(linha[5]),
                    'bloqueio': str(linha[6]),
                    'mensagem_unificada': str(linha[0]) + ';' + str(linha[1]) + ';' + str(linha[2]) + ';' + str(
                        linha[3]) + ';' + str(linha[4]) + ';' + str(linha[5])
                })

            return {'messages': messages}
        else:
            return {"message": "Nao ha mensagens liberadas armazenadas."}, 200


class MessagesBloqueadas(Resource):

    def get(self):
        connection = sqlite3.connect('banco.db')
        cursor = connection.cursor()

        resultado = cursor.execute('SELECT * FROM messages WHERE bloqueio = 1')
        if resultado:
            messages = []
            for linha in resultado:
                messages.append({
                    'message_id': str(linha[0]),
                    'ddd': str(linha[1]),
                    'celular': str(linha[2]),
                    'operadora': str(linha[3]),
                    'horario_envio': str(linha[4]),
                    'mensagem': str(linha[5]),
                    'bloqueio': str(linha[6]),
                    'mensagem_unificada': str(linha[0]) + ';' + str(linha[1]) + ';' + str(linha[2]) + ';' + str(linha[3]) + ';' + str(linha[
                        4]) + ';' + str(linha[5])
                })

            return {'messages': messages}
        else:
            return {"message": "Nao ha mensagens bloqueadas armazenadas."}, 200

