

### API para validacão de mensagens e retorno ###

* Periodo : 11/08/2020
* Version 1.00
* Ambiente : Desktop

### Ferramentas usadas ###

* Linguagem: Python 3.6
* Framework : Flask com SQLALchemy
* Plataforma : Linux/Windows
* Banco de dados - SQLite
* IDE : Pycharm 2019

Foi usado o Flask para construcao da API em localhost com um banco simples SQLlite, e este é criado no momento da primeira execucão, 
tambem foi usado SQLAlchemy como ORM para mapeamento de banco afim de mostrar a facilidade conjunta dos dois para um caso de expansao dessa solucao e 
tambem usado a lib Request para fazer requisicao externa para verificar os telefones na blacklist.Usando o arquivo requeiments.txt tambem facilita de 
coletar as dependencias para execucao. O codigo foi construido em Orientacao Objeto mas sem uso especifico de algum Design Pattern .
Usado tambem Decorator para especificar os metodos de classe na camada model e tambem no uso SQLALchemy.

### Autor  ###

* Alan Azevedo Bancks

### Contato ###

* E-mail: dsalan@hotmail.com

### Observacões ###

Link original : https://github.com/pgmais/teste-backend 

Fiz algumas mudancas em relacão ao que foi pedido inicialmente. Transformei o servico de valicão de mensagens 
em formato de API por ser mais pratico de uso em cenario real, a não se que ele fosse executado dentro de uma outra
aplicacao rodando em Linux. De qualquer forma, pelo menos vai servir para dar uma otica diferente ao problema e ainda faz
o que foi proposto no desafio, mudando apenas o formato de entrada, que ao invez de ser apenas um string que talvez fosse lida
do buffer, agora é um JSON lendo de uma unica chave


Favor desconsiderar a ortografia pois nao acertei bem o layout do meu teclado :( 

### Instrucões de uso ###

* Protocolo : RESTFull
* Formato de menssagem : JSON

* Requisicao POST
* Padrão de mensagem de envio 

{
	"message": "bff58d7b-8b4a-456a-b852-5a3e000c0e63;12;996958849;NEXTEL;18:24:03;sapien sapien non mi integer ac neque duis bibendum"
}

* URL : http://localhost:5000/message
* header : application/json

* Tipo de retorno para mensagens bloqueadas :

{
    "id_messagem": "b4f58d7b-8b4a-456a-b852-5a3e000c0e68",
    "ddd": 12,
    "celular": 996958849,
    "operadora": "NEXTEL",
    "horario_envio": "18:24:03",
    "menssagem": "sapien sapie neque duis bibendum",
    "bloqueio": true,
    "mensagem_unificada": "b4f58d7b-8b4a-456a-b852-5a3e000c0e68;12;996958849;NEXTEL;18:24:03;sapien sapie neque duis bibendum",
    "message_return": "b4f58d7b-8b4a-456a-b852-5a3e000c0e68;3"
}

* Retorno para mensagem liberada

* Retorno para mensagem que ja existe

{
    "message": "message_id 'b4f58d7b-8b4a-456a-b852-5a3e000c0e63' ja existe."
}

----
* Recuperar mensagens bloqueadas
* Requisicão GET
* http://localhost:5000/messages/bloqueadas

* Retorno :


{
"messages": [
  {
    "message_id": "bff58d7b-8b4a-456a-b852-5a3e000c0e63",
    "ddd": "12",
    "celular": "996958849",
    "operadora": "NEXTEL",
    "horario_envio": "18:24:03",
    "mensagem": "sapien sapien non mi integer ac neque duis bibendum",
    "bloqueio": "1",
    "mensagem_unificada": "bff58d7b-8b4a-456a-b852-5a3e000c0e63;12;996958849;NEXTEL;18:24:03;sapien sapien non mi integer ac neque duis bibendum"
},
  {
    "message_id": "b4f58d7b-8b4a-456a-b852-5a3e000c0e63",
    "ddd": "12",
    "celular": "996958849",
    "operadora": "NEXTEL",
    "horario_envio": "18:24:03",
    "mensagem": "sapien sapien non mi integer ac neque duis bibendum",
    "bloqueio": "1",
    "mensagem_unificada": "b4f58d7b-8b4a-456a-b852-5a3e000c0e63;12;996958849;NEXTEL;18:24:03;sapien sapien non mi integer ac neque duis bibendum"
    },
  {
    "message_id": "b4f58d7b-8b4a-456a-b852-5a3e000c0e68",
    "ddd": "12",
    "celular": "996958849",
    "operadora": "NEXTEL",
    "horario_envio": "18:24:03",
    "mensagem": "sapien sapie neque duis bibendum",
    "bloqueio": "1",
    "mensagem_unificada": "b4f58d7b-8b4a-456a-b852-5a3e000c0e68;12;996958849;NEXTEL;18:24:03;sapien sapie neque duis bibendum"
    }
],
}

----
* Recuperar menssagens liberadas
* Requisicão GET
* http://localhost:5000/messages/liberadas

* Retorno :

{
"messages": [
  {
    "message_id": "n4751610d-8u3b-458a-b052-5a3e000c0e68",
    "ddd": "12",
    "celular": "996958849",
    "operadora": "NEXTEL",
    "horario_envio": "18:24:03",
    "mensagem": "sapien sapie neque duis bibendum",
    "bloqueio": "0",
    "mensagem_unificada": "n4751610d-8u3b-458a-b052-5a3e000c0e68;12;996958849;NEXTEL;18:24:03;sapien sapie neque duis bibendum"
    },
  {
    "message_id": "n4751610d-8u3b-458a-b052-5a3e020c0e68",
    "ddd": "12",
    "celular": "996958849",
    "operadora": "NEXTEL",
    "horario_envio": "18:24:03",
    "mensagem": "sapien sapie neque duis bibendum",
    "bloqueio": "0",
    "mensagem_unificada": "n4751610d-8u3b-458a-b052-5a3e020c0e68;12;996958849;NEXTEL;18:24:03;sapien sapie neque duis bibendum"
    }
],
}


----




###  Regras mapeadas ###

* Mensagens com telefone inválido deverão ser bloqueadas(DDD+NUMERO) - OK
* Mensagens que estão na blacklist deverão ser bloqueadas; (ver blacklist) - OK 
* Mensagens para o estado de São Paulo deverão ser bloqueadas; - OK
* Mensagens com agendamento após as 19:59:59 deverão ser bloqueadas; - OK
* As mensagens com mais de 140 caracteres deverão ser bloqueadas; - OK
* Caso possua mais de uma mensagem para o mesmo destino, apenas a mensagem apta com o menor horário deve ser considerada - [
Esta regra nao ficou muito clara, pois segundo o exemplo informado, mesmo que fosse enviado varias mensagens de uma vez,
ainda sim ele teria que olhar linha por linha e levando isso em consideracao todas a mensagens seriam processadas,
mesmo a mais recente ou mais antiga, respeitando apenas como identificado unico a message_id, a nao ser que fosse feito um filtro validador do conjunto total da entrada, ai Ex: se no conjunto lido houve-se mais de uma menssagem para o mesmo numero, so uma eh considerada e o restante descartado. Mas quem garante que a mensagem antiga com a nova nao fossem parte de uma unica mensagem? Descartando menssagem assim nao seria um risco ? ];
* O id_broker será definido conforme a operadora; (ver broker x operadora) - OK

### Adicionais ###

* Se ja houver uma mensagem com id registrado, descartar
* Listar todas as mensagens que foram liberadas
* Listar todas as mensagens que foram bloqueadas
* O json de retorno mostra tanto a string solicitada , como os dados enviados na requisicao , para validacao